package main

import (
	"bytes"
	"fmt"
	"io"
	"strings"
)

const alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"

func main() {
	w := &bytes.Buffer{}
	DrawDiamond(w, "Z")
	fmt.Print(w)
}
func DrawDiamond(w io.Writer, s string) {
	charIndex := strings.Index(alphabet, s)
	var topHalf string
	for i := 0; i <= charIndex; i++ {
		letter := string(alphabet[i])
		line := Padding(s, i)
		line += PrintLetter(letter)
		if i > 0 {
			line += InBetweenSpace(letter)
			line += PrintLetter(letter)
		}
		line += Padding(s, i)
		line += fmt.Sprintln()
		topHalf += line
	}
	bottomHalf := reverse(topHalf)
	bottomHalf = dropFirstLine(bottomHalf)
	diamond := topHalf + bottomHalf
	fmt.Fprintf(w, "%s\n", diamond)
}
func PrintLetter(s string) string {
	return strings.ToUpper(s)
}

func InBetweenSpace(l string) string {
	r := strings.Index(alphabet, l)*2 - 1
	if r >= 0 {
		return strings.Repeat(" ", r)
	}
	return ""
}
func Padding(l string, i int) string {
	end := strings.Index(alphabet, l)
	if i == end {
		return ""
	}
	return strings.Repeat(" ", end-i)
}

func reverse(input string) string {
	lines := strings.Split(input, "\n")
	for i := 0; i < len(lines)/2; i++ {
		j := len(lines) - i - 1
		lines[i], lines[j] = lines[j], lines[i]
	}
	return strings.Join(lines, "\n")
}

func dropFirstLine(in string) string {
	in = strings.Trim(in, "\n")
	lines := strings.Split(in, "\n")
	return strings.Join(lines[1:], "\n")
}
