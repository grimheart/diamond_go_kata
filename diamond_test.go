package main

import (
	"bytes"
	"fmt"
	"testing"

	approvals "github.com/approvals/go-approval-tests"
	"github.com/stretchr/testify/assert"
)

func TestDrawDiamond(t *testing.T) {
	for i, letter := range []rune("ABCDEFGHIJKLMNOPQRSTUVWXYZ") {
		t.Run(fmt.Sprintf("Should draw a diamond up to level %d for a %s given", i, string(letter)), func(t *testing.T) {
			b := &bytes.Buffer{}
			DrawDiamond(b, string(letter))
			approvals.VerifyString(t, b.String())
		})
	}
}

func TestInBetweenSpace(t *testing.T) {
	t.Run("Should be zero for A", func(t *testing.T) {
		assert.Empty(t, InBetweenSpace("A"))
	})
	t.Run("Should be 5 for D", func(t *testing.T) {
		assert.Equal(t, "     ", InBetweenSpace("D"))
	})
}

func TestPadding(t *testing.T) {
	t.Run("Should be 0 for D and 3", func(t *testing.T) {
		assert.Empty(t, Padding("D", 3))
	})
	t.Run("Should be 2 for D and 1", func(t *testing.T) {
		assert.Equal(t, "  ", Padding("D", 1))
	})
	t.Run("Should be 3 for D and 0", func(t *testing.T) {
		assert.Equal(t, "   ", Padding("D", 0))
	})
}
func TestReverse(t *testing.T) {
	t.Run("Should reverse topHalf of a diamon", func(t *testing.T) {
		topHalf := `  A  
 B B 
C   C`
		want := `C   C
 B B 
  A  `
		assert.Equal(t, want, reverse(topHalf))
	})
}

func TestDropFirstLine(t *testing.T) {
	t.Run("Should drop the first line", func(t *testing.T) {
		given := `C   C
 B B 
  A  `
		want := ` B B 
  A  `
		assert.Equal(t, want, dropFirstLine(given))
	})
}
